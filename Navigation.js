import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import HomeScreen from "./components/HomeScreen";
import DeshtokTwo from "./components/DeshtokTwo";
import DeshtokThree from "./components/DeshtokThree";
import CalendarEvents from "./components/CalendarEvents/CalendarEvents";
import DetailEvets from "./components/CalendarEvents/detailEvent/detailEvent";
import ListaPeople from "./components/CalendarEvents/detailEvent/listUsuarios";



//import MaterialComunityIcons from 'react-native-vector-icons/MaterialComunityIcons';

const Tab = createBottomTabNavigator();

const MyTabs = () => {
  return (
    <Tab.Navigator
    initialRouteName="Home"
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        style: {
          position: 'absolute',
          elevation: 0,
          color: '#fff',
          backgroundColor: '#000000',
          with: '375pt',
          height: '200pt',
          
      }
    }}
    >
      
      <Tab.Screen 
        name="Home" 
        component={HomeScreen}
        screenOptions={{
          headerShown: false,
        }}
        options={{
            tabBarIcon: ({ focused }) => (
            <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
            >
            <Text
            style={{color: focused ? '#FFF843' : '#000000'}}>Home </Text>
            </View>
            )   
        }}
      />
      <Tab.Screen 
        name="Sig." 
        component={DeshtokTwo}
        options={{
            tabBarIcon: ({ focused }) => (
            <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
            >
            <Text
            style={{color: focused ? '#FFF843' : '#000000'}}>Sig.</Text>
            </View>
            )   
        }}
      />
      <Tab.Screen 
        name="Tres" 
        component={DeshtokThree}
        options={{
            tabBarIcon: ({ focused }) => (
            <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
            >
            <Text
            style={{color: focused ? '#FFF843' : '#000000'}}>Tres.</Text>
            </View>
            )   
        }}
      />
      <Tab.Screen 
        name="Calendar" 
        component={CalendarEvents}
        options={{
            tabBarIcon: ({ focused }) => (
            <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
            >
            <Text
            style={{color: focused ? '#FFF843' : '#000000'}}>Home Calendar</Text>
            </View>
            )   
        }}
      />
      <Tab.Screen 
        name="DetailEvet" 
        component={DetailEvets}
        options={{
            tabBarIcon: ({ focused }) => (
            <View style={{ alignItems: 'center', justifyContent: 'center', top: 10}}
            >
            <Text
            style={{color: focused ? '#FFF843' : '#000000'}}>detailcal</Text>
            </View>
            )   
        }}
      />

      
    </Tab.Navigator>

    
    
    );  
    
 
}

export default MyTabs;
