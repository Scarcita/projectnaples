import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList } from "react-native";

//// IMAGENES////////
import ImgPortada from '../assets/Img/women.svg'
const HomeScreen = ({navigation}) => {


  return ( 
        <View>
          <ImgPortada
              style={styles.imgWomen}
            />

          <Text style={styles.title}>
           Help to shape the business climate in your local area
          </Text>

          <Text style={styles.description}>
          Find all our events to register easily more opportunities to make real connections
          </Text>

          <TouchableOpacity
            onPress={() => navigation.navigate("DeshtokTwo")}
              style={styles.button}
          >
            <Text
                style={styles.textButton}
            >
                Continue
            </Text>
          </TouchableOpacity>


        </View>
      
    
  )
    
}

export default HomeScreen;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#000000',
    
    
  },
  imgWomen: {
    textAlign: "center",
    alignSelf: 'center',
    width: '100%',
    height: '100%'
  },
  title: {
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 35,
    marginTop: 10,
    textAlign: "center"
  },

  description: {
    color: '#000000',
    //fontWeight: 'bold',
    fontSize: 14,
    marginTop: 10,
    textAlign: "center"
  },

  button: {
    backgroundColor: "#FDB827",
    marginTop: 20,
    width: 320,
    height: 58,
    alignSelf: 'center',
    borderRadius: 17,

  },
  textButton: {
    textAlign: 'center',
    fontSize: 24,
    color: '#FFFFFF',
    paddingTop: 13,
    //fontFamily: 'Prompt_600SemiBold',
  }

});
