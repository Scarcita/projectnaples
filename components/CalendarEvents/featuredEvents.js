import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";

////IMAGENES///////////
import ImgCard from '../../assets/Calendar/post.svg'
import ImgNotification from '../../assets/Calendar/notification.svg'
const FeatureEvents = () => {

    return(
        <View>
            <View style= {styles.twoSeccion}>
            <View>
                <Text style={styles.titleEvent}>
                 Featured Events
                </Text>
            </View>
            <View >
                <Text style= {styles.view}>
                    View all
                </Text>
            </View>
          </View>
          <View style={styles.card}>
            <View>
              <ImgCard style={styles.ImgCard}></ImgCard>
              <View style={styles.flexObject}>
                <View style={styles.cardDate}>
                  <View style={styles.cardContent}>
                    <Text style={styles.nameDay}>Mon</Text>
                    <Text style={styles.dayDate}>19</Text>
                    <Text style={styles.nameMonth}>Dec</Text>
                  </View>
                </View>
                <View>
                 <ImgNotification style={styles.notification}></ImgNotification>
                </View>
              </View>
            </View>
            <View style={styles.seccionText}>
              <Text style={styles.titleCard}>
                Greater Naples Chamber Jingle & Mingle
              </Text>
              <Text style={styles.descriptionCard}>
                Networking Event
              </Text>
            </View>
          </View>
        </View>

    )
}

const styles = StyleSheet.create ({
    container: {
      flex: 1,  
    },
    twoSeccion: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop: 20,
    },
    titleEvent: {
      color: '#000000',
      fontSize: 17,
      fontWeight: 'bold'
    },
    view: {
      color: '#747474',
      fontSize: 14,
      fontWeight: '300'
    },
    card: {
      width: 272,
      height: 320,
      backgroundColor: '#fff',
      alignSelf: 'center',
      marginTop: 22,
      borderBottomRightRadius: 17,
      borderBottomLeftRadius: 17,
    },
    ImgCard: {
      alignSelf: 'center',
    },
    flexObject: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      position: 'absolute'
    },
    cardDate: {
      width: 68, 
      height: 68, 
      backgroundColor: '#00B5C3', 
      borderRadius: 9,
      marginLeft: 5,
      marginTop: 5
    },
  
    cardContent: {
      alignSelf: 'center',
      textAlign: 'center',
      padding: 5
      
    },
    nameDay: {
      color: '#fff',
      fontWeight: '400',
      fontSize: 12,
    },
    dayDate: {
      color: '#fff',
      fontWeight: '700',
      fontSize: 24,
    },
    nameMonth: {
      color: '#fff',
      fontWeight: '400',
      fontSize: 12,
    },
    notification:{
      marginLeft: 165,
      marginTop: 25
  
    },
    seccionText:{
      marginLeft: 15,
      marginRight:15
    },
    titleCard: {
      fontSize: 14,
      fontWeight: '700',
      marginTop: 15
    },
    descriptionCard: {
      fontSize: 14,
      fontWeight: '300',
      marginTop: 10
    },
   
  });

export default FeatureEvents;