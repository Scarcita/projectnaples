import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";


const UpcomingEvents = () => {

    return(

        <View>
            <View style={styles.threeSeccion}>
              <View>
                  <Text style={styles.titleUpcomingEvent}>
                    Upcoming Events
                  </Text>
              </View>
              <View >
                  <Text style= {styles.UpcomingView}>
                      View all
                  </Text>
              </View>
            </View>
            <View style={styles.flexEvent}>
                 <View style={styles.descriptionEvent}>
                    <View style={styles.cardDateEvent}>
                        <View style={styles.cardContentEvent}>
                            <Text style={styles.nameDayEvent}>Mon</Text>
                            <Text style={styles.dayDateEvent}>19</Text>
                            <Text style={styles.nameMonthEvent}>Dec</Text>
                        </View>
                    </View>
                    <View style={styles.informationEvent}>
                        <Text style={styles.nameEvent}>Holiday Crafts for Kids</Text>
                        <Text style={styles.dateEvent}>10:00 AM to 05:00 PM</Text>
                        <Text style={styles.locationEvent}>Rookery Bay</Text>
                    </View>
                </View>
                <View style={{alignSelf: 'center'}}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate("DeshtokTwo")}
                        style={styles.button}
                    >
                    <Text
                        style={styles.textButton}
                    >
                        + Inf
                    </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>

    )
}

export default UpcomingEvents;

const styles = StyleSheet.create ({
    threeSeccion: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop: 30,
    },
    titleUpcomingEvent: {
      color: '#000000',
      fontSize: 17,
      fontWeight: 'bold'
    },
    UpcomingView: {
      color: '#747474',
      fontSize: 14,
      fontWeight: '300'
    },
  /////////////////////////
    flexEvent: {
      flexDirection: 'row',
      justifyContent: 'space-between',
  
    },
    descriptionEvent:{
      flexDirection: 'row',
      marginTop: 30
  
    },
    cardDateEvent: {
      width: 68, 
      height: 68, 
      backgroundColor: '#00B5C3', 
      borderRadius: 9,
    },
  
    cardContentEvent: {
      alignSelf: 'center',
      textAlign: 'center',
      padding: 5
      
    },
    nameDayEvent: {
      color: '#fff',
      fontWeight: '400',
      fontSize: 12,
    },
    dayDateEvent: {
      color: '#fff',
      fontWeight: '700',
      fontSize: 24,
    },
    nameMonthEvent: {
      color: '#fff',
      fontWeight: '400',
      fontSize: 12,
    },
    informationEvent: {
      marginLeft: 15,
      alignSelf: 'center'
    },
    nameEvent: {
      fontSize: 14,
      color: '#000000',
      fontWeight: '700'
    },
    dateEvent: {
      fontSize: 12,
      color: '#747474',
      fontWeight: '500'
  
    },
    locationEvent: {
      fontSize: 12,
      color: '#747474',
      fontWeight: '500'
  
    },
    button: {
      backgroundColor: "#D9D9D9",
      marginTop: 20,
      width: 78,
      height: 26,
      alignSelf: 'center',
      alignSelf: 'center',
      borderRadius: 7,
  
    },
    textButton: {
      textAlign: 'center',
      fontSize: 14,
      fontWeight: '700',
      color: '#FFFFFF',
      alignSelf: 'center',
      paddingTop: 4
      //fontFamily: 'Prompt_600SemiBold',
    }
  
  });